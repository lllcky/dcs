#########
Lucky DCS
#########

.. image:: dcs.png

****************
Установка с нуля
****************

Перед переключением на новую винду сделать скрин из joystick gremlin об подключенных устройствах vjoy, скопировать из guid.
Также можно сохранить лог запуска Joystick gremlin - там есть информация о vjoy.

Установить следующие драйверы и программы:

* драйвер для вебкамеры *./CL-Eye-Driver-5.3.0.0341.exe* и других физических устройств
* Opentrack (см. ниже)
* vjoy
* midi2vjoy (см. ниже)
* joystick gremlin (лучше использовать портативный)
* VPC WarBRD утилиту - возможно потребует установка новой прошивки (делается автоматом из утилиты)
* NI Controller Editor
* NI Kontrol X1 driver

Opentrack
=========

`Opentrack Releases <https://github.com/opentrack/opentrack/releases>`_

Установить опентрек, файл конфигруации *lucky-opentrack.ini* подсунуть в директорию, где находится дефолтный конфиг. В настройках камеры параметр **gain** сделать на минимум (галочку *auto* убрать); **exposure** установить примерно на 30-40%. Отрегулировать настройки, кривые по месту.

NI Kontroller X1 MK1
====================

Загрузить необходимые профили. *Раздел дополняется*

midi2voy
========

После установки *vjoy* устанавливается *midi2vjoy*. Для этого требуется установить:

* python3
* pip (идёт в дистрибутиве python)
* midi2vjoy - https://github.com/c0redumb/midi2vjoy
* Интернет

Установка
---------

Скачать всё необходимое. Установить midi2vjoy::

   PS C:\l\git-midi2vjoy> python setup.py install
   running install
   C:\p\Python\Python313\Lib\site-packages\setuptools\_distutils\cmd.py:79: SetuptoolsDeprecationWarning: setup.py install is deprecated.
   !!

           ********************************************************************************
           Please avoid running ``setup.py`` directly.
           Instead, use pypa/build, pypa/installer or other
           standards-based tools.

           See https://blog.ganssle.io/articles/2021/10/setup-py-deprecated.html for details.
           ********************************************************************************

   !!
     self.initialize_options()
   C:\p\Python\Python313\Lib\site-packages\setuptools\_distutils\cmd.py:79: EasyInstallDeprecationWarning: easy_install command is deprecated.
   !!

           ********************************************************************************
           Please avoid running ``setup.py`` and ``easy_install``.
           Instead, use pypa/build, pypa/installer or other
           standards-based tools.

           See https://github.com/pypa/setuptools/issues/917 for details.
           ********************************************************************************

   !!
     self.initialize_options()
   running bdist_egg
   running egg_info
   writing midi2vjoy.egg-info\PKG-INFO
   writing dependency_links to midi2vjoy.egg-info\dependency_links.txt
   writing entry points to midi2vjoy.egg-info\entry_points.txt
   writing requirements to midi2vjoy.egg-info\requires.txt
   writing top-level names to midi2vjoy.egg-info\top_level.txt
   reading manifest file 'midi2vjoy.egg-info\SOURCES.txt'
   adding license file 'LICENSE'
   writing manifest file 'midi2vjoy.egg-info\SOURCES.txt'
   installing library code to build\bdist.win-amd64\egg
   running install_lib
   running build_py
   copying midi2vjoy\midi2vjoy.py -> build\lib\midi2vjoy
   creating build\bdist.win-amd64\egg
   creating build\bdist.win-amd64\egg\midi2vjoy
   copying build\lib\midi2vjoy\midi2vjoy.py -> build\bdist.win-amd64\egg\midi2vjoy
   byte-compiling build\bdist.win-amd64\egg\midi2vjoy\midi2vjoy.py to midi2vjoy.cpython-313.pyc
   creating build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\PKG-INFO -> build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\SOURCES.txt -> build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\dependency_links.txt -> build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\entry_points.txt -> build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\not-zip-safe -> build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\requires.txt -> build\bdist.win-amd64\egg\EGG-INFO
   copying midi2vjoy.egg-info\top_level.txt -> build\bdist.win-amd64\egg\EGG-INFO
   creating 'dist\midi2vjoy-0.1-py3.13.egg' and adding 'build\bdist.win-amd64\egg' to it
   removing 'build\bdist.win-amd64\egg' (and everything under it)
   Processing midi2vjoy-0.1-py3.13.egg
   removing 'c:\p\python\python313\lib\site-packages\midi2vjoy-0.1-py3.13.egg' (and everything under it)
   creating c:\p\python\python313\lib\site-packages\midi2vjoy-0.1-py3.13.egg
   Extracting midi2vjoy-0.1-py3.13.egg to c:\p\python\python313\lib\site-packages
   Adding midi2vjoy 0.1 to easy-install.pth file
   Installing midi2vjoy-script.py script to C:\p\Python\Python313\Scripts
   Installing midi2vjoy.exe script to C:\p\Python\Python313\Scripts

   Installed c:\p\python\python313\lib\site-packages\midi2vjoy-0.1-py3.13.egg
   Processing dependencies for midi2vjoy==0.1
   Searching for pygame==2.6.1
   Best match: pygame 2.6.1
   Adding pygame 2.6.1 to easy-install.pth file

   Using c:\p\python\python313\lib\site-packages
   Finished processing dependencies for midi2vjoy==0.1

Проверка
--------

Подключить midi устройство, убедиться, что драйвер для него установлен. Выполнить команду для проверки вывода значений midi::

   PS C:\Users\lucky> midi2vjoy.exe -t
   pygame 2.6.1 (SDL 2.28.4, Python 3.13.2)
   Hello from the pygame community. https://www.pygame.org/contribute.html
   Input MIDI devices:
   1 Automap MIDI
   2 Automap Propellerhead
   3 Automap Propellerhead Mixer
   Select MIDI device to test: 1
   Opening MIDI device: 1
   Device opened for testing. Use ctrl-c to quit.
   [[[192, 1, 0, 0], 7488]]
   [[[192, 3, 0, 0], 7519]]
   [[[192, 2, 0, 0], 8190]]
   [[[192, 1, 0, 0], 8346]]
   [[[192, 3, 0, 0], 8940]]
   [[[192, 5, 0, 0], 9080]]
   [[[192, 4, 0, 0], 9474]]
   [[[192, 3, 0, 0], 9758]]
   [[[192, 1, 0, 0], 10037]]
   [[[176, 26, 2, 0], 11314]]
   [[[176, 26, 4, 0], 11329]]
   [[[176, 26, 6, 0], 11335]]
   [[[176, 26, 6, 0], 11341]]
   [[[176, 26, 4, 0], 11361]]

************
CLI commands
************

Запуск скриптов для пробоса midi в vjoy::

   cd C:\l\git-dcs\midi2vjoy\
   .\lucky_midi_a10c.py -c .\midi2vjoy_translation.conf -m 1 -v
   .\lucky_midi_reds.py -c .\midi2vjoy_translation.conf -m 1 -v
   .\lucky_midi_ka50.py -c .\midi2vjoy_translation.conf -m 1 -v
   .\lucky_midi_av8b.py -c .\midi2vjoy_translation.conf -m 1 -v

В интерфейсе DCS устройства vjoy сортированы по guid, поэтому в данном случае будет сначала второй, потом первый vjoy device:

* Right VPC Stick WarBRD {3510CBD0-9674-11EE-8003-444553540000}
* vJoy Device #1 128 кнопок {52CD8270-9689-11EE-8002-444553540000}
* vJoy Device #2 120 кнопок {52CDA980-9689-11EE-8004-444553540000}

Предыдущие guid из профилей 2023 г.:

* vJoy Device {0ED07E40-363A-11eb-8002-444553540000}.diff.lua
* vJoy Device {8609D3F0-37C8-11eb-8003-444553540000}.diff.lua


Замена профилей устройств
=========================

Для Joystick Gremlin в xml-конфиге заменить все вхождения старого guid на новый для RIGHT VPC, vjoy1, vjoy2.

vjoy1 - это виртуальный джостик, на который перенаправлются команды с других устройств. Он не сконфигурирован как Input.
vjoy2 сконфигурирован как Input, на него отправляются команды с midi2vjoy.

В профилях для каждого типа ВС требуется только замена имён файлов, содержащих guid. 
Пример::

   find -name "*8609D3F0-37C8-11eb-8003-444553540000*" -exec rename -nv "8609D3F0-37C8-11eb-8003-444553540000" "52CDA980-9689-11EE-8004-444553540000" {} \;
   find -name "*0ED07E40-363A-11eb-8002-444553540000*" -exec rename -nv "0ED07E40-363A-11eb-8002-444553540000" "52CD8270-9689-11EE-8002-444553540000" {} \;

   old="old-guid"
   new="new-guid"
   find -name "*$old*" -exec rename -nv $old $new {} \;

Альтернативный вариант - подгрузить профили ввода для каждого типа ВС прямо в DCS.

Профили находятся в `C:\Users\lucky\Saved Games\DCS\Config\Input`.

Февраль 2025. Новые guid RightVPC, vJoy1, 2::

   {FC423CE0-C1DC-11EF-8001-444553540000}
   {CFDFB0E0-EB96-11EF-8002-444553540000}
   {57F5E2A0-EC47-11EF-8003-444553540000}

См. *gui.txt*.


Порядок подключения ПО для старта окружения DCS
===============================================

* Подключение физических устройств: наушники с клипсой, камера, контроллер, джойстик
* Opentrack
* midi2vjoy в терминале
* NI Controller Editor
* Joystick gremlin: активировать, переключить нужный профиль
