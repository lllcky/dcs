#  midi2vjoy.py
#
#  Copyright 2017  <c0redumb>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys, os, time, traceback
import ctypes
from optparse import OptionParser
import pygame.midi
# import winreg
from datetime import datetime

# Constants
# Axis mapping
axis = {'X': 0x30, 'Y': 0x31, 'Z': 0x32, 'RX': 0x33, 'RY': 0x34, 'RZ': 0x35,
        'SL0': 0x36, 'SL1': 0x37, 'WHL': 0x38, 'POV': 0x39}

# Globals
options = None

def midi_test():
    n = pygame.midi.get_count()

    # List all the devices and make a choice
    print('Input MIDI devices:')
    for i in range(n):
        info = pygame.midi.get_device_info(i)
        if info[2]:
            print(i, info[1].decode())
    d = int(input('Select MIDI device to test: '))

    # Open the device for testing
    try:
        print('Opening MIDI device:', d)
        m = pygame.midi.Input(d)
        print('Device opened for testing. Use ctrl-c to quit.')
        while True:
            while m.poll():
                print(m.read(1))
            time.sleep(0.1)
    except:
        m.close()

def read_conf(conf_file):
    '''Read the configuration file'''
    table = {}
    vids = []
    with open(conf_file, 'r') as f:
        for l in f:
            if len(l.strip()) == 0 or l[0] == '#':
                continue
            ### convert a config line to the list
            fs = l.split()
            key = (int(fs[0]), int(fs[1]))
            ### 177 used in Novation Nocturn midi devide. Not used by me
            if fs[0] == '177':
                val = (int(fs[2]), int(fs[3]))
            else:
                val = (int(fs[2]), fs[3])
            table[key] = val
            vid = int(fs[2])
            if not vid in vids:
                vids.append(vid)
    return (table, vids)

def joystick_run():
    # Process the configuration file
    if options.conf == None:
        print('Must specify a configuration file')
        return
    try:
        if options.verbose:
            print('Opening configuration file:', options.conf)
        (table, vids) = read_conf(options.conf)
        print(table)
        print(vids)
    except:
        print('Error processing the configuration file:', options.conf)
        return

### Parking break
    # Getting the MIDI device ready
    if options.midi == None:
        print('Must specify a MIDI interface to use')
        return
    try:
        if options.verbose:
            print('Opening MIDI device:', options.midi)
        midi = pygame.midi.Input(options.midi)
        print('postok')
    except:
        print('Error opting MIDI device:', options.midi)
        return

    # Load vJoysticks
    try:
        # Load the vJoy library
        # Load the registry to find out the install location
        #vjoyregkey = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, 'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{8E31F76F-74C3-47F1-9550-E041EEDC5FBB}_is1')
        #installpath = winreg.QueryValueEx(vjoyregkey, 'InstallLocation')
        installpath = 'C:\\Program Files\\vJoy'
        #winreg.CloseKey(vjoyregkey)
        print('installpath =', installpath)
        dll_file = os.path.join(installpath, 'x64', 'vJoyInterface.dll')
        print('dll_file', dll_file)
        vjoy = ctypes.WinDLL(dll_file)
        print('vjoy.GetvJoyVersion()', vjoy.GetvJoyVersion())
        print('vids', vids)
        # print('table', table)

        # Getting ready
        for vid in vids:
            if options.verbose:
                print('Acquiring vJoystick:', vid)
            assert(vjoy.AcquireVJD(vid) == 1)
            assert(vjoy.GetVJDStatus(vid) == 0)
            vjoy.ResetVJD(vid)
    except:
        #traceback.print_exc()
        print('Error initializing virtual joysticks')
        return

    try:
        if options.verbose:
            print('Ready. Use ctrl-c to quit.')
        while True:
            while midi.poll():
                ipt = midi.read(1)
                # print(ipt)
                key = tuple(ipt[0][0][0:2])
                reading = ipt[0][0][2]
                # Check that the input is defined in table
                if not key in table:
                    continue
                opt = table[key]
                if options.verbose:
                    print(datetime.now().strftime("%H:%M:%S"), 'key =', key, '\u2192 opt =', opt, '\b, reading =', reading)
                if key[0] == 176 and key[1] in range(0, 8):
                    # A slider input
                    # Check that the output axis is valid
                    # Note: We did not check if that axis is defined in vJoy
                    if not opt[1] in axis:
                        continue
                    if key[1] == 4 or 5:
                        ### Center RZ axis for rudes 50% instead of 51% ###
                        ### << 8 - shift left to 8 bit
                        reading = (reading - 0) << 8
                    else:
                        ### others make 100% in midi velocity 127
                        reading = (reading + 1) << 8
                    vjoy.SetAxis(reading, opt[0], axis[opt[1]])
                ### USED vjoy2 buttons 1-8 ###
                ### CC39
                ### visenie LA+T
                elif key[0] == 176 and key[1] == 39 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 39 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 83)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 83)

                ### CC12
                ### Parking break
                elif key[0] == 176 and key[1] == 12 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 12 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 56)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 56)

                ### CC16; CC2 = Shift + CC16
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 16 and reading == 127:
                    #
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 16 and reading == 1:
                    #
                    vjoy.SetBtn(reading, opt[0], 2)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 2)

                ### CC17; CC1 = Shift + CC17
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 17 and reading == 127:
                    #
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 17 and reading == 1:
                    #
                    vjoy.SetBtn(reading, opt[0], 1)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 1)

                ### CC26
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 26 and reading == 127:
                    # Ka-50 ABRIS brightness
                    vjoy.SetBtn(reading, opt[0], 26)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 26)
                elif key[0] == 176 and key[1] == 26 and reading == 1:
                    # Ka-50 ABRIS brightness
                    vjoy.SetBtn(reading, opt[0], 6)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 6)

                ### CC27
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 27 and reading == 127:
                    # Ka-50 ABRIS data
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 27 and reading == 1:
                    # Ka-50 ABRIS data
                    vjoy.SetBtn(reading, opt[0], 7)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 7)

                ### CC61
                ### absolute mode in x1_mk1: 4-pos. selector
                elif key[0] == 176 and key[1] == 61 and reading == 1:
                    # A10C AAP Steer PT - Other
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 61 and reading == 2:
                    # A10C AAP Steer PT - Pos info
                    vjoy.SetBtn(reading, opt[0], 3)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 3)
                elif key[0] == 176 and key[1] == 61 and reading == 3:
                    # A10C AAP Steer PT - Steer info
                    vjoy.SetBtn(reading, opt[0], 4)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 4)
                elif key[0] == 176 and key[1] == 61 and reading == 4:
                    # A10C AAP Steer PT - WP info
                    vjoy.SetBtn(reading, opt[0], 5)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 5)

                ### CC28
                elif key[0] == 176 and key[1] == 28 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 28 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 72)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 72)

                ### CC35
                elif key[0] == 176 and key[1] == 35 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 35 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 79)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 79)

                ### CC30
                elif key[0] == 176 and key[1] == 30 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 30 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 74)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 74)

                ### CC31
                elif key[0] == 176 and key[1] == 31 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 31 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 75)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 75)

                ### CC32
                elif key[0] == 176 and key[1] == 32 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 32 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 76)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 76)

                ### CC34
                elif key[0] == 176 and key[1] == 34 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 34 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 78)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 78)

                ### CC38
                elif key[0] == 176 and key[1] == 38 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 38 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 82)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 82)

                ### CC41
                elif key[0] == 176 and key[1] == 41 and reading == 127:
                    # button has pressed
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 41 and reading == 0:
                    # button has released
                    vjoy.SetBtn(127, opt[0], 85)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 85)

                ### CC60
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 60 and reading == 127:
                    #
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 60 and reading == 1:
                    #
                    vjoy.SetBtn(reading, opt[0], 91)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 91)

                ### CC61
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 61 and reading == 127:
                    #
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 61 and reading == 1:
                    #
                    vjoy.SetBtn(reading, opt[0], 89)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 89)

                ### CC70
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 70 and reading == 127:
                    #
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 70 and reading == 1:
                    #
                    vjoy.SetBtn(reading, opt[0], 90)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 90)

                ### CC71
                ### relative mode in x1_mk1: rotate counterwise reading = 127, clockwise reading = 1
                elif key[0] == 176 and key[1] == 71 and reading == 127:
                    #
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], int(opt[1]))
                elif key[0] == 176 and key[1] == 71 and reading == 1:
                    #
                    vjoy.SetBtn(reading, opt[0], 8)
                    time.sleep(0.1)
                    vjoy.SetBtn(0, opt[0], 8)


                elif key[0] == 176:
                    # A button input in x1_mk1 in the gate mode
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                elif key[0] == 177:
                    # A button input
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
                elif key[0] == 128:
                    # A button off input
                    vjoy.SetBtn(reading, opt[0], int(opt[1]))
            time.sleep(0.1)
    except:
        #traceback.print_exc()
        pass

    # Relinquish vJoysticks
    for vid in vids:
        if options.verbose:
            print('Relinquishing vJoystick:', vid)
        vjoy.RelinquishVJD(vid)

    # Close MIDI device
    if options.verbose:
        print('Closing MIDI device')
    midi.close()

def main():
    # parse arguments
    parser = OptionParser()
    parser.add_option("-t", "--test", dest="runtest", action="store_true",
                      help="To test the midi inputs")
    parser.add_option("-m", "--midi", dest="midi", action="store", type="int",
                      help="File holding the list of file to be checked")
    parser.add_option("-c", "--conf", dest="conf", action="store",
                      help="Configuration file for the translation")
    parser.add_option("-v", "--verbose",
                          action="store_true", dest="verbose")
    parser.add_option("-q", "--quiet",
                          action="store_false", dest="verbose")
    global options
    (options, args) = parser.parse_args()

    pygame.midi.init()

    if options.runtest:
        midi_test()
    else:
        joystick_run()

    pygame.midi.quit()

if __name__ == '__main__':
    main()
