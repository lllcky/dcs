local diff = {
	["keyDiffs"] = {
		["d1558pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = ",",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "RWR Knob CCW",
		},
		["d1559pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = ".",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "RWR Knob CW",
		},
		["d3248pnilu3248cd24vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num4",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "ODU Option 4",
		},
		["d3249pnilu3249cd24vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num1",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "ODU Option 5",
		},
		["d3250pnilu3250cd24vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num8",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "ODU Option 1",
		},
		["d3251pnilu3251cd24vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num5",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "ODU Option 2",
		},
		["d3252pnilu3252cd24vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num2",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "ODU Option 3",
		},
		["d3294pnilu3294cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "C",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "UFC Timer Function Button",
		},
		["d3296pnilu3296cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "0",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "UFC Target-Of-Opportunity Function Button",
		},
		["d3302pnilu3302cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num1",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 1 Button",
		},
		["d3303pnilu3303cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num2",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 2/N Button",
		},
		["d3304pnilu3304cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num3",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 3 Button",
		},
		["d3305pnilu3305cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num*",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC Clear Button",
		},
		["d3306pnilu3306cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num4",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 4/W Button",
		},
		["d3307pnilu3307cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num5",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 5 Button",
		},
		["d3308pnilu3308cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num6",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 6/E Button",
		},
		["d3310pnilu3310cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num7",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 7 Button",
		},
		["d3311pnilu3311cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num8",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 8/S Button",
		},
		["d3312pnilu3312cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num9",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 9 Button",
		},
		["d3313pnilu3313cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num-",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC - Button",
		},
		["d3314pnilu3314cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "NumEnter",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC Enter Button",
		},
		["d3315pnilu3315cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num0",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC 0 Button",
		},
		["d3316pnilu3316cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "Num.",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC . Button",
		},
		["d3317pnilu3317cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "O",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "UFC ON/OFF Toggle Button",
		},
		["d3319pnilu3319cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "T",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "UFC TACAN Function Button",
		},
		["d3321pnilu3321cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "W",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "UFC Weapons Function Button",
		},
		["d3324pnilu3324cd23vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "H",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "UFC Altimeter Function Button",
		},
		["d3407pnilu3407cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "1",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 1 Pushbutton",
		},
		["d3409pnilu3409cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "2",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 2 Pushbutton",
		},
		["d3411pnilu3411cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "3",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 3 Pushbutton",
		},
		["d3413pnilu3413cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "4",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 4 Pushbutton",
		},
		["d3415pnilu3415cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "5",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 5 Pushbutton",
		},
		["d3417pnilu3417cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "6",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 6 Pushbutton",
		},
		["d3419pnilu3419cd29vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "7",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Station 7 Pushbutton",
		},
		["d3421pnilunilcd11vd0.4vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "F",
					["reformers"] = {
						[1] = "LAlt",
					},
				},
			},
			["name"] = "INS Mode: IFA",
		},
		["d3489pnilunilcd28vd1vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "W",
					["reformers"] = {
						[1] = "LCtrl",
					},
				},
			},
			["name"] = "Parking Brake OFF",
		},
		["d3512pnilunilcd33vd0vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "P",
					["reformers"] = {
						[1] = "LShift",
					},
				},
			},
			["name"] = "Anti Collision Lights OFF",
		},
		["d3543pnilunilcd33vd0vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "L",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "Landing Light APP/OFF",
		},
		["d3544pnilunilcd28vd0vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "S",
				},
			},
			["name"] = "Anti-skid NWS/ON Toggle",
		},
	},
}
return diff