local diff = {
	["keyDiffs"] = {
		["d175pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "L",
					["reformers"] = {
						[1] = "RShift",
					},
				},
			},
			["name"] = "АНО, 10%/30%/100%/Выкл",
			["removed"] = {
				[1] = {
					["key"] = "L",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
		},
	},
}
return diff