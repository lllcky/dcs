local diff = {
	["keyDiffs"] = {
		["d100pnilu1627cdnilvdnilvpnilvunil"] = {
			["name"] = "Цель, захват(дозаправка в воздухе, сброс/рассоединенние)",
			["removed"] = {
				[1] = {
					["key"] = "Enter",
				},
			},
		},
		["d350pnilu351cdnilvdnilvpnilvunil"] = {
			["name"] = "Пуск/сброс (гашетка подвесн. вооруж.)",
			["removed"] = {
				[1] = {
					["key"] = "Space",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
		},
	},
}
return diff