local diff = {
	["axisDiffs"] = {
		["a2003cdnil"] = {
			["name"] = "Педали",
			["removed"] = {
				[1] = {
					["key"] = "JOY_RZ",
				},
			},
		},
		["a2087cdnil"] = {
			["name"] = "РОШ",
			["removed"] = {
				[1] = {
					["key"] = "JOY_Z",
				},
			},
		},
	},
}
return diff