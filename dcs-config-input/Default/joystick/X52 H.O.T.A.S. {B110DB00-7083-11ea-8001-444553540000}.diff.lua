local diff = {
	["axisDiffs"] = {
		["a2004cdnil"] = {
			["name"] = "Тяга",
			["removed"] = {
				[1] = {
					["key"] = "JOY_SLIDER1",
				},
			},
		},
	},
	["keyDiffs"] = {
		["dnilp36unilcdnilvdnilvpnilvunil"] = {
			["name"] = "Взгляд в центр",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN5",
				},
			},
		},
	},
}
return diff