local diff = {
	["axisDiffs"] = {
		["a2003cdnil"] = {
			["changed"] = {
				[1] = {
					["filter"] = {
						["curvature"] = {
							[1] = 0,
						},
						["deadzone"] = 0.01,
						["invert"] = false,
						["saturationX"] = 1,
						["saturationY"] = 1,
						["slider"] = false,
					},
					["key"] = "JOY_RZ",
				},
			},
			["name"] = "Руль направления, (педали)",
		},
	},
	["keyDiffs"] = {
		["dnilp36unilcdnilvdnilvpnilvunil"] = {
			["name"] = "Взгляд в центр",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN5",
				},
			},
		},
	},
}
return diff