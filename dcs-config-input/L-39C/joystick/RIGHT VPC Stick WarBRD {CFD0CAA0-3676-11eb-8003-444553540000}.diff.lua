local diff = {
	["axisDiffs"] = {
		["a2003cdnil"] = {
			["name"] = "Руль направления, (педали)",
			["removed"] = {
				[1] = {
					["key"] = "JOY_RZ",
				},
			},
		},
		["a2004cdnil"] = {
			["name"] = "Тяга",
			["removed"] = {
				[1] = {
					["key"] = "JOY_Z",
				},
			},
		},
	},
}
return diff