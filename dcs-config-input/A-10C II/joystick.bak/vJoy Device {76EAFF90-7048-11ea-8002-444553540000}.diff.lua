local diff = {
	["axisDiffs"] = {
		["a2012cdnil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_RY",
				},
			},
			["name"] = "Камера в кабине, зум (абсолют)",
		},
	},
	["keyDiffs"] = {
		["d350pnilu351cdnilvdnilvpnilvunil"] = {
			["name"] = "Пуск/сброс (гашетка подвесн. вооруж.)",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN2",
				},
			},
		},
		["d387pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN34",
				},
			},
			["name"] = "Автопилот, удержание высоты барометрической - вкл\\выкл",
		},
		["d554pnilu638cdnilvdnilvpnilvunil"] = {
			["name"] = "HOTAS гашетка 2 позиция (Стрельба)",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN1",
				},
			},
		},
		["d562pnilu606cdnilvdnilvpnilvunil"] = {
			["name"] = "HOTAS Управление носовым колесом",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN4",
				},
			},
		},
		["d636pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN35",
				},
			},
			["name"] = "Автопилот, удержание высоты и курса - вкл\\выкл",
		},
		["d637pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN33",
				},
			},
			["name"] = "Автопилот, следовать по маршруту - вкл\\выкл",
		},
	},
}
return diff