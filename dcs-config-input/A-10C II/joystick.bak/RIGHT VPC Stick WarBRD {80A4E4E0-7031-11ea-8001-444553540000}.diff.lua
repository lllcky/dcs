local diff = {
	["keyDiffs"] = {
		["d554pnilu638cdnilvdnilvpnilvunil"] = {
			["name"] = "HOTAS гашетка 2 позиция (Стрельба)",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN1",
				},
			},
		},
		["d562pnilu606cdnilvdnilvpnilvunil"] = {
			["name"] = "HOTAS Управление носовым колесом",
			["removed"] = {
				[1] = {
					["key"] = "JOY_BTN4",
				},
			},
		},
	},
}
return diff