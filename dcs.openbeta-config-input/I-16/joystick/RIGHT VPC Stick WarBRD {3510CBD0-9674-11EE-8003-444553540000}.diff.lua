local diff = {
	["keyDiffs"] = {
		["d961pnilu962cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN25",
				},
			},
			["name"] = "Тормоз колес, левый вкл./выкл.",
		},
		["d963pnilu964cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN23",
				},
			},
			["name"] = "Тормоз колес, правый вкл./выкл.",
		},
	},
}
return diff