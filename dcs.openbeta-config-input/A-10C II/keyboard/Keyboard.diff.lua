local diff = {
	["keyDiffs"] = {
		["d1307pnilu1307cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "Num*",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
				[2] = {
					["key"] = "F",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC кнопка FUNC",
		},
		["d1309pnilu1309cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "Back",
					["reformers"] = {
						[1] = "LCtrl",
						[2] = "LShift",
					},
				},
			},
			["name"] = "UFC кнопка очистки ввода (CLR)",
		},
		["d300pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "U",
				},
			},
			["name"] = "Освещение кабины",
			["removed"] = {
				[1] = {
					["key"] = "L",
				},
			},
		},
		["d539pnilu543cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "K",
				},
			},
			["name"] = "HOTAS Coolie переключатель вверх",
			["removed"] = {
				[1] = {
					["key"] = "U",
				},
			},
		},
		["d542pnilu543cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "L",
				},
			},
			["name"] = "HOTAS Coolie переключатель вправо",
			["removed"] = {
				[1] = {
					["key"] = "K",
				},
			},
		},
	},
}
return diff