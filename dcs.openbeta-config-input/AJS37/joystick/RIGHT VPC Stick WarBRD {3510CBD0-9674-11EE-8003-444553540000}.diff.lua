local diff = {
	["keyDiffs"] = {
		["d3002pnilunilcd23vd0vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN11",
				},
			},
			["name"] = "A1 Mode",
		},
		["d3090pnilu3090cd22vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN5",
				},
			},
			["name"] = "Reference button",
		},
		["d3300pnilu3300cd2vd0vpnilvu1"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN20",
				},
				[2] = {
					["key"] = "JOY_BTN28",
				},
			},
			["name"] = "Trigger Safety Bracket - SAFE else ARM (2-way Switch)",
		},
		["d350pnilu351cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN4",
				},
			},
			["name"] = "Пуск/сброс подвесного вооружения",
		},
		["dnilp93u215cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN9",
				},
			},
			["name"] = "Триммер, кнюппель - крен влево",
		},
		["dnilp94u215cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN7",
				},
			},
			["name"] = "Триммер, кнюппель - крен вправо",
		},
		["dnilp95u215cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN8",
				},
			},
			["name"] = "Триммер, кнюппель - тангаж положительный",
		},
		["dnilp96u215cdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN6",
				},
			},
			["name"] = "Триммер, кнюппель - тангаж отрицательный ",
		},
	},
}
return diff