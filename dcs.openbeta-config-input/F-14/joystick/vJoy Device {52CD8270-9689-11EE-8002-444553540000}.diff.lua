local diff = {
	["keyDiffs"] = {
		["dnilp2019u2019cdnilvdnilvp-1vu0"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN103",
				},
			},
			["name"] = "Триммер вниз",
		},
		["dnilp2019u2019cdnilvdnilvp1vu0"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN102",
				},
			},
			["name"] = "Триммер вверх",
		},
		["dnilp2020u2020cdnilvdnilvp-1vu0"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN101",
				},
			},
			["name"] = "Trim Roll Left Wing Down",
		},
		["dnilp2020u2020cdnilvdnilvp1vu0"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN104",
				},
			},
			["name"] = "Trim Roll Right Wing Down",
		},
	},
}
return diff