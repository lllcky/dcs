windowPlacement = {
    ["y"] = 0,
    ["x"] = 0,
    ["w"] = 1920,
    ["h"] = 1080,
}

monitors = {
    [1] = {
        ["rcMonitor"] = {
            ["top"] = 0,
            ["right"] = 1920,
            ["left"] = 0,
            ["bottom"] = 1080,
        },
        ["dwFlags"] = 1,
        ["rcWork"] = {
            ["top"] = 0,
            ["right"] = 1920,
            ["left"] = 0,
            ["bottom"] = 1040,
        },
    },
}