preset = {
	["graphics"] = {
		["AA"] = "OFF",
		["BlurFlatShadows"] = 0,
		["ColorGradingLUT"] = 0,
		["DLSS_PerfQuality"] = 1,
		["DOF"] = 0,
		["LODmult"] = 1,
		["LensEffects"] = 3,
		["MSAA"] = 1,
		["SSAO"] = 0,
		["SSLR"] = 0,
		["SSS"] = 0,
		["Scaling"] = 0.66,
		["ScreenshotExt"] = "jpg",
		["Sharpening"] = 0,
		["Upscaling"] = "OFF",
		["anisotropy"] = 4,
		["aspect"] = 1.3333333333333,
		["box_mouse_cursor"] = false,
		["canopyReflections"] = 1,
		["chimneySmokeDensity"] = 4,
		["civTraffic"] = "",
		["clouds"] = 1,
		["clutterMaxDistance"] = 0,
		["cockpitGI"] = 1,
		["defaultFOV"] = 78,
		["effects"] = 3,
		["flatTerrainShadows"] = 0,
		["forestDetailsFactor"] = 1,
		["forestDistanceFactor"] = 0.5,
		["fullScreen"] = false,
		["heatBlr"] = 0,
		["height"] = 1080,
		["lights"] = 2,
		["maxFPS"] = 180,
		["messagesFontScale"] = 1,
		["motionBlur"] = 0,
		["motionBlurAmount"] = 1,
		["multiMonitorSetup"] = "1camera",
		["outputGamma"] = 2.2,
		["preloadRadius"] = 150000,
		["rainDroplets"] = 0,
		["scaleGui"] = 1,
		["sceneryDetailsFactor"] = 1,
		["secondaryShadows"] = 0,
		["shadowTree"] = false,
		["shadows"] = 2,
		["sync"] = false,
		["terrainTextures"] = "min",
		["textures"] = 0,
		["useDeferredShading"] = 1,
		["visibRange"] = "High",
		["volumetricLights"] = 1,
		["water"] = 2,
		["width"] = 1920,
	},
	["views"] = {
		["cockpit"] = {
			["avionics"] = 0,
		},
	},
}
