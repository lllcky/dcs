local diff = {
	["keyDiffs"] = {
		["d3016pnilu3016cd12vd1vpnilvu0"] = {
			["added"] = {
				[1] = {
					["key"] = "X",
					["reformers"] = {
						[1] = "RAlt",
					},
				},
			},
			["name"] = "РУС, блокировка в нейтральном положении",
			["removed"] = {
				[1] = {
					["key"] = "X",
					["reformers"] = {
						[1] = "LAlt",
					},
				},
			},
		},
		["d309pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "Home",
					["reformers"] = {
						[1] = "LWin",
					},
				},
			},
			["name"] = "Автозапуск",
		},
		["d310pnilunilcdnilvdnilvpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "End",
					["reformers"] = {
						[1] = "LWin",
					},
				},
			},
			["name"] = "Автоостанов",
		},
	},
}
return diff