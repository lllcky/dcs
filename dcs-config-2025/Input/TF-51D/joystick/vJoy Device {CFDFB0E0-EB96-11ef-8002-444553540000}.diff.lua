local diff = {
	["keyDiffs"] = {
		["d3014pnilunilcd12vd1vpnilvunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN115",
				},
				[2] = {
					["key"] = "JOY_BTN100",
				},
			},
			["name"] = "Триммер, сброс",
		},
		["dnilp3011unilcd12vdnilvp-0.1vunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN111",
				},
				[2] = {
					["key"] = "JOY_BTN101",
				},
			},
			["name"] = "Триммер элерона - влево",
		},
		["dnilp3011unilcd12vdnilvp0.1vunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN114",
				},
				[2] = {
					["key"] = "JOY_BTN104",
				},
			},
			["name"] = "Триммер элерона - вправо",
		},
		["dnilp3012unilcd12vdnilvp-0.05vunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN112",
				},
				[2] = {
					["key"] = "JOY_BTN102",
				},
			},
			["name"] = "Триммер - на кабрирование",
		},
		["dnilp3012unilcd12vdnilvp0.05vunil"] = {
			["added"] = {
				[1] = {
					["key"] = "JOY_BTN113",
				},
				[2] = {
					["key"] = "JOY_BTN103",
				},
			},
			["name"] = "Триммер - на пикирование",
		},
	},
}
return diff